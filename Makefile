REG_ADDRESS=registry.gitlab.com
USERNAME=supernami
VERSION=1.0

percona-build:
	docker-compose -f percona.yml build

percona-tag:
	docker tag percona:latest ${REG_ADDRESS}/${USERNAME}/docker-percona-images:base-${VERSION} && \
	docker tag ${REG_ADDRESS}/${USERNAME}/docker-percona-images:base-${VERSION} ${REG_ADDRESS}/${USERNAME}/docker-percona-images:base-latest

percona-push:
	docker push ${REG_ADDRESS}/${USERNAME}/docker-percona-images:base-${VERSION} && \
	docker push ${REG_ADDRESS}/${USERNAME}/docker-percona-images:base-latest

percona-build-config:
	docker-compose -f percona-config.yml build
